#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# 2018-2019 � BIM & Scan� Ltd.
# See 'README.md' in the project root for more information.
#
import os

from conans import CMake, \
                   tools

from conans.model.conan_file import ConanFile
from conans.errors import ConanInvalidConfiguration


class Cereal(ConanFile):
    name = "cereal"
    version = "1.2.2"
    license = "BSD-3"
    url = "https://bitbucket.org/headcount_bimandscan/conan-cereal"
    description = "A C++11 library for serialisation."
    generators = "cmake"
    author = "Neil Hyland <neil.hyland@bimandscan.com>"
    homepage = "http://uscilab.github.io/cereal/"
    no_copy_source = True

    _src_dir = f"{name}-{version}"

    settings = "os", \
               "compiler", \
               "build_type", \
               "arch"

    options = {
                  "thread_safe": [
                                     True,
                                     False
                                 ]
              }

    default_options = "thread_safe=False"

    exports = "../LICENCE.md"

    def _valid_cppstd(self):
        return self.settings.compiler.cppstd == "17" or \
               self.settings.compiler.cppstd == "gnu17" or \
               self.settings.compiler.cppstd == "20" or \
               self.settings.compiler.cppstd == "gnu20" or \
               self.settings.compiler.cppstd == "11" or \
               self.settings.compiler.cppstd == "gnu11" or \
               self.settings.compiler.cppstd == "14" or \
               self.settings.compiler.cppstd == "gnu14"

    def config_options(self):
        if not self._valid_cppstd():
            raise ConanInvalidConfiguration("Library requires C++11 or higher!")

        if self.settings.os == "Windows":
            del self.options.fPIC

    def source(self):
        zip_name = f"{self._src_dir}.zip"
        src_cmakefile = f"{self._src_dir}/CMakeLists.txt"

        tools.download(f"https://github.com/USCiLab/cereal/archive/v{self.version}.zip",
                       zip_name)

        tools.unzip(zip_name)
        os.unlink(zip_name)

        # Patch CMake configuration:
        tools.replace_in_file(src_cmakefile,
                              "project (cereal)",
                              "project(cereal CXX)\ninclude(\"${CMAKE_BINARY_DIR}/conanbuildinfo.cmake\")\nconan_basic_setup()")

    def configure_cmake(self):
        cmake = CMake(self)

        # Configure CMake library build:
        cmake.definitions["CMAKE_INSTALL_PREFIX"] = self.package_folder
        cmake.definitions["JUST_INSTALL_CEREAL"] = True

        return cmake

    def build(self):
        cmake = self.configure_cmake()
        cmake.configure(source_folder = self._src_dir)
        cmake.build()

    def package(self):
        cmake = self.configure_cmake()
        cmake.install()

        self.copy("LICENSE",
                  "licenses",
                  self._src_dir)

    def package_info(self):
        self.cpp_info.includedirs = [
                                        "include"
                                    ]

        if self.options.thread_safe:
            if self.settings.os != "Windows":
                self.cpp_info.libs.append("pthread")

                self.cpp_info.cppflags.extend([
                                                  "-pthread",
                                                  "-DCEREAL_THREAD_SAFE=1"
                                              ])

                self.cpp_info.cflags.extend([
                                                "-pthread",
                                                "-DCEREAL_THREAD_SAFE=1"
                                            ])
            else:
                self.cpp_info.cppflags.append("/DCEREAL_THREAD_SAFE=1")
                self.cpp_info.cflags.append("/DCEREAL_THREAD_SAFE=1")

    def package_id(self):
        self.info.header_only()
