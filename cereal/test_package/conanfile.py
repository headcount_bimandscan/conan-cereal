#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# 2018-2019 � BIM & Scan� Ltd.
# See 'README.md' in the project root for more information.
#
import os

from conans.model.conan_file import ConanFile

from conans import CMake, \
                   tools


class PkgTest_Cereal(ConanFile):
    name = "pkgtest_cereal"
    generators = "cmake"
    author = "Neil Hyland <neil.hyland@bimandscan.com>"

    settings = "os", \
               "compiler", \
               "build_type", \
               "arch"

    requires = "cereal/1.2.2@bimandscan/unstable"

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()

    def imports(self):
        pass

    def test(self):
        os.chdir("bin")
        self.run(f".{os.sep}{self.name}")
