/*
 * 2018-2019 © BIM & Scan® Ltd.
 * See 'README.md' in the project root for more information.
 */
#include <cstdlib>
#include <iostream>

#include <cereal/cereal.hpp>


struct MyData
{
    inline MyData() = default;
    inline ~MyData() = default;


    std::size_t a,
                b,
                c;


    template<typename ARCHIVE>
    inline void serialize(ARCHIVE& p_archive)
    {
        p_archive(a,
                  b,
                  c);
    }
};



int main(int p_arg_count,
         char** p_arg_vector)
{
    std::cout << "'Cereal' package test (compilation, linking, and execution).\n";

    MyData d1,
           d2;

    std::cout << "'Cereal' package works!" << std::endl;
    return EXIT_SUCCESS;
}
