# BIM & Scan® Third-Party Library (Cereal)

![BIM & Scan](BimAndScan.png "BIM & Scan® Ltd.")

Conan build script for [Cereal](http://uscilab.github.io/cereal/), a C++11 library for serialisation.

Requires a C++11 compiler (or higher).

Supports version 1.2.2 (stable).
